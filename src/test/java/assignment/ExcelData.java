package assignment;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelData {
	public static Object[][] getTestData() throws IOException {

		File f = new File("/home/dilip/Documents/TestData/TestData1.xlsx");
		FileInputStream fs = new FileInputStream(f);
		XSSFWorkbook book = new XSSFWorkbook(fs);

		XSSFSheet sheet = book.getSheetAt(0);
		Object[][] data = new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
		for (int i = 0; i < sheet.getLastRowNum(); i++) {
			for (int j = 0; j < sheet.getRow(0).getLastCellNum(); j++) {

				data[i][j] = sheet.getRow(i + 1).getCell(j).getStringCellValue();
			}

		}
		return data;

	}
	
	public static Object[][] getTestData1() throws IOException {

		File f = new File("/home/dilip/Documents/TestData/TestData1.xlsx");
		FileInputStream fs = new FileInputStream(f);
		XSSFWorkbook book = new XSSFWorkbook(fs);

		XSSFSheet sheet = book.getSheetAt(1);
		Object[][] data = new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
		for (int i = 0; i < sheet.getLastRowNum(); i++) {
			for (int j = 0; j < sheet.getRow(0).getLastCellNum(); j++) {

				data[i][j] = sheet.getRow(i + 1).getCell(j).getStringCellValue();
			}

		}
		return data;

	}


}